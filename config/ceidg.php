<?php
return [
    'api_key' => env('CEIDG_API_KEY'),                  // API key received upon registration on https://datastore.ceidg.gov.pl/
    'mode' => env('CEIDG_MODE', 'sandbox')      // `production` or `sandbox`
];
