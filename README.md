# CEIDG API wrapper for Laravel 5.6+

It's just Laravel wrapper for [sigrun/ceidg-api](https://github.com/sigrundev/ceidg-api) library.

## Installation

```shell
composer require netborg/laravel-ceidg-sdk
```

## Configuration

Add these settings to your `.env` file:
```php

CEIDG_API_KEY=[YOUR_API_KEY]        // API key received upon registration on https://datastore.ceidg.gov.pl/
CEIDG_MODE=production               // `production` or `sandbox`
```

## Example of usage

```php

// call service by it's class name
app()->make(CeidgApi\CeidgApi::class);

// or by it's alias
app()->make('ceidg');

// or inject as dependency
class Example {
    
    /**
    * @var \CeidgApi\CeidgApi
    */
    protected $ceidg;

    /**
    * Example constructor.
    * @param \CeidgApi\CeidgApi $ceidg
    */
    public function __construct(\CeidgApi\CeidgApi $ceidg) {
        $this->ceidg = $ceidg;
    }
}
```
More usage examples on sigrun's page: [https://github.com/sigrundev/ceidg-api](https://github.com/sigrundev/ceidg-api)
