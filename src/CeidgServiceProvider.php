<?php
declare(strict_types=1);

namespace Netborg\CEIDG;

use CeidgApi\CeidgApi;
use Illuminate\Support\ServiceProvider;

class CeidgServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(CeidgApi::class, function () {
            return new CeidgApi(
                config('ceidg.api_key'),
                'production' === config('ceidg.mode') ? false : true
            );
        });
        $this->app->bind('ceidg', CeidgApi::class);
    }
    

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/ceidg.php', 'ceidg');

        if ($this->app->runningInConsole()) {
            // publish CEIDG config file
            $this->publishes([
                __DIR__.'/../config/ceidg.php' => config_path('ceidg.php'),
            ], 'ceidg-config');
        }
    }
}
